package singleton;
import java.sql.*;

public class SingletonPatternMovies {
	private static Connection conn=null;

	private SingletonPatternMovies() {
		
	}
	public static Connection getSingletonMovies() {
		try {
			if(conn==null) {
				Class.forName("com.mysql.cj.jdbc.Driver");
				conn=(Connection)DriverManager.getConnection("jdbc:mysql://localhost:3306/database3?autoReconnect=true&useSSL=false","root","Coronavirus@1");
				System.out.println("database connected successfully\n");
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return conn;
	}
	
	
	

}